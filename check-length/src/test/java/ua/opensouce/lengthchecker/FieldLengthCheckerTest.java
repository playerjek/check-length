package ua.opensouce.lengthchecker;

import org.junit.Before;
import org.junit.Test;
import ua.opensource.lengthcheker.FieldLengthChecker;
import ua.opensource.lengthcheker.bean.SomeBean;
import ua.opensource.lengthcheker.exceptions.FieldLengthException;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Asmolov on 21.06.2015.
 */
public class FieldLengthCheckerTest {

    private SomeBean bean;
    protected String descriptionGood = "����� ����� ���� ��� ��������, ��� � ��������";
    protected String urlGood = "http://veeeeeeeeeeeeeeeeeeerrryyyyyyyyyyyyy.long.url.com.ua";
    protected String addressGood = "X-Town, Makarova street, build 22";
    protected String cardNumGood = "555555555555555555";

    protected String descriptionBad = "����� ����� ���� ��� ��������, ��� � ��������, ����� ����� ���� ��� ��������, ��� � ��������";
    protected String urlBad = "http://veeeeeeeeeeeeeeeeeeerrryyyyyyyyyyyyy.long.url.com.ua";
    protected String addressBad = "Solar system, Planet Earth, X - Town, Makarova street, build 22";
    protected String cardNumBad = "555555555555555555111111";

    @Before
    public void prepareBean(){
        bean = new SomeBean();
    }

    @Test
    public void testCheckLength() throws NoSuchMethodException {
        System.out.println("Test normal size fields");
        checkFields(false);

        System.out.println("Test Address, length: " + addressGood.length());
        bean.setAddress(addressBad);
        checkFields(true);
        bean.setAddress(addressGood);

        System.out.println("Test CardNum, length: " + cardNumBad.length());
        bean.setCardNum(cardNumBad);
        checkFields(true);
        bean.setCardNum(cardNumGood);

        System.out.println("Test Url, length: " + urlBad.length());
        bean.setUrl(urlBad);
        checkFields(true);
        bean.setUrl(urlGood);

        System.out.println("Test Description, length: " + descriptionBad.length());
        bean.setDescription(descriptionBad);
        checkFields(true);
        bean.setDescription(descriptionGood);
    }

    public void checkFields(boolean isExcept) {
        try {
            FieldLengthChecker.getChecker().checkLength(bean, bean.getClass());
            System.out.println("Checked");
        } catch (Exception e) {
            assertTrue(isExcept);
            assertTrue(e instanceof FieldLengthException);
            //TODO: add asserts on every Exception
        }
    }
}
