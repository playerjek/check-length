package ua.opensource.lengthcheker.exceptions;

/**
 * Created by Asmolov on 21.06.2015.
 */
public class FieldIsNullException extends FieldLengthException {

    public FieldIsNullException(String message) {
        super(message);
    }
}
