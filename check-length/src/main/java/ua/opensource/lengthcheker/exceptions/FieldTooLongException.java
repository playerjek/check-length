package ua.opensource.lengthcheker.exceptions;

/**
 * Created by Asmolov on 21.06.2015.
 */
public class FieldTooLongException extends FieldLengthException {

    public FieldTooLongException(String message) {
        super(message);
    }
}
