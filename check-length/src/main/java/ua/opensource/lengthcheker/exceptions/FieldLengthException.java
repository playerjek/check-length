package ua.opensource.lengthcheker.exceptions;

/**
 * Created by Asmolov on 21.06.2015.
 */
public class FieldLengthException extends Exception {
    public FieldLengthException(String message) {
        super(message);
    }
}
