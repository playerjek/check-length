package ua.opensource.lengthcheker;

import ua.opensource.lengthcheker.annotation.Length;
import ua.opensource.lengthcheker.exceptions.FieldIsNullException;
import ua.opensource.lengthcheker.exceptions.FieldLengthException;
import ua.opensource.lengthcheker.exceptions.FieldTooLongException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;

/**
 * Created by Asmolov on 21.06.2015.
 */
public class FieldLengthChecker {

    private static final FieldLengthChecker checker = new FieldLengthChecker();

    public static FieldLengthChecker getChecker(){
        return checker;
    }

    private FieldLengthChecker(){}

    public void checkLength(Object arg, Class clazz) throws FieldLengthException {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field: fields){
            if (!field.getType().equals(String.class)) continue;
            Length fieldLength = field.getAnnotation(Length.class);
            if (fieldLength != null) {
                try {
                    field.setAccessible(true);
                    String value = (String) field.get(arg);
                    if (fieldLength.min()==1 && value == null) throw new FieldIsNullException("Param "+field.getName()+" is null");
                    if (value == null || value.isEmpty()) continue;
                    if (fieldLength.binary()) {
                        try {
                            byte[] bytes = value.getBytes("UTF-8");
                            if (bytes.length > fieldLength.max()) {
                                byte[] result = new byte[fieldLength.max()];
                                System.arraycopy(bytes, 0, result, 0, fieldLength.max());
                                field.set(arg, new String(result, "UTF-8"));
                            }
                        } catch (UnsupportedEncodingException e) {
                            System.err.println("Couldn't cut " + field.getName());
                        }
                    } else if (value.length() > fieldLength.max()) {
                        if (fieldLength.cut()) {
                            value = value.substring(0, fieldLength.max());
                            field.set(arg, value);
                        } else
                            throw new FieldTooLongException("Field " + field.getName() + " has invalid length: " + value.length());
                    }
                } catch (FieldLengthException e) {
                    throw e;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    field.setAccessible(false);
                }
            }
        }
        if (clazz.getSuperclass() != null)
            checkLength(arg, clazz.getSuperclass());
    }
}
