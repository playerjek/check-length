package ua.opensource.lengthcheker.bean;

import ua.opensource.lengthcheker.annotation.Length;

/**
 * Created by Asmolov on 21.06.2015.
 */
public class SomeBean {
    @Length(max = 1000, binary = true)
    protected String description = "";
    @Length(min = 1, max = 30, cut = true)
    protected String url = "http://veeeeeeeeeeeeeeeeeeerrryyyyyyyyyyyyy.long.url.com.ua";
    @Length(max = 50)
    protected String address = "X-Town, Makarova street, build 22";
    @Length(min = 1, max = 20)
    protected String cardNum ="55555555555555";
    protected int number = 3;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
