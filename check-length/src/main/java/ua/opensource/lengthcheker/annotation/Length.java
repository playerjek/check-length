package ua.opensource.lengthcheker.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Asmolov on 21.06.2015.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Length {
    int min() default 0;
    int max() default 0;
    boolean cut() default false;
    boolean binary() default false;
}
